extern crate glib;
use self::glib::ObjectExt;

pub struct ConnectedSignalHandler<T: ObjectExt> {
    instance: T,
    signal_handler_id: Option<glib::SignalHandlerId>
}

impl<T: ObjectExt> ConnectedSignalHandler<T> {
    pub fn new(instance: T, signal_handler_id: glib::SignalHandlerId) -> Self {
        Self {
            instance,
            signal_handler_id: Some(signal_handler_id)
        }
    }
}

impl<T: ObjectExt> Drop for ConnectedSignalHandler<T> {
    fn drop(&mut self) {
        self.instance.disconnect(self.signal_handler_id.take().unwrap());
    }
}
